# Binary Calculator

**Objective**

In this challenge, we implement a calculator that uses binary numbers.

**Task**

Implement a simple calculator that performs the following operations on binary numbers: addition, subtraction, multiplication, and division. Note that division operation must be integer division only; for example, , , and .

The calculator's initial state must look like this:

![](images/binaryCalculator.png "Binary Calculatot")
