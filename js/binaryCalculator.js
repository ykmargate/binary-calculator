var binary1 = '';
var binary2 = '';
var oper = '';
var res = '';
var obj = document.getElementById('res');

function add01(x) {
    console.log('add0');
    if(oper == ''){
        binary1 = binary1+x;
        obj.innerText = binary1;
    }
    else {
        binary2 = binary2 + x;
        obj.innerText = obj.innerText + x;
    }
}

function clr() {
    console.log('clear');
    binary1 = '';
    binary2 = '';
    operation = '';
    res = '';
    obj.innerText = '';
    document.getElementById('btnEql').disabled = false;
    document.getElementById('btn0').disabled = false;
    document.getElementById('btn1').disabled = false;
    document.getElementById('btnSum').disabled = false;
    document.getElementById('btnSub').disabled = false;
    document.getElementById('btnMul').disabled = false;
    document.getElementById('btnDiv').disabled = false;
}

function result() {
    console.log('result');
    var decimal1 = parseInt(binary1, 2);
    var decimal2 = parseInt(binary2, 2);
    var result;
    switch (oper){
        case '+':
            result = decimal1+decimal2;
            break;
        case '-':
            result = decimal1-decimal2;
            break;
        case '*':
            result = decimal1*decimal2;
            break;
        case '/':
            result = decimal1/decimal2;
            break;
    }
    console.log(result + ' ' + result.toString(2));
    obj.innerText = result.toString(2);
    document.getElementById('btnEql').disabled = true;
    document.getElementById('btn0').disabled = true;
    document.getElementById('btn1').disabled = true;
}

function action(x) {
    console.log(x);
    oper = x;
    binary1 = binary1 + x;
    obj.innerText = binary1;
    document.getElementById('btnSum').disabled = true;
    document.getElementById('btnSub').disabled = true;
    document.getElementById('btnMul').disabled = true;
    document.getElementById('btnDiv').disabled = true;
}

